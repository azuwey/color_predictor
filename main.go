package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"

	"github.com/golang-ui/nuklear/nk"
	"github.com/kpango/glg"
	"gitlab.com/azuwey/color_predictor/gfxs"
)

const (
	version        = "0.0.0"
	maxCount uint8 = 1
)

func init() {
	// This is needed to arrange that main() runs on main thread.
	runtime.LockOSThread()
}

func main() {
	// Initialize logger
	infolog := glg.FileWriter(fmt.Sprintf("/log/info.%s.log",
		time.Now().Format("2006_01_02_1504")), 0666)
	defer infolog.Close()

	defer glg.Get().
		SetMode(glg.BOTH).
		AddLevelWriter(glg.INFO, infolog).
		AddLevelWriter(glg.ERR, infolog).
		AddLevelWriter(glg.FAIL, infolog).
		AddLevelWriter(glg.FATAL, infolog).
		Stop()

	// Set random number seed
	rand.Seed(time.Now().UTC().UnixNano())

	// Set the defualt state
	state := &gfxs.State{
		Color:           nk.NkRgb(rand.Int31n(256), rand.Int31n(256), rand.Int31n(256)),
		Count:           0,
		ShouldExit:      false,
		RequestNewColor: false,
	}

	go func(state *gfxs.State) {
		fpsTicker := time.NewTicker(time.Second / 1000)
		for !state.ShouldExit {
			select {
			case <-fpsTicker.C:
				if state.RequestNewColor {
					generateNewColor(state)
				}
				break
			}
		}
	}(state)

	// Initialize GLFW
	gfxs.InitGLFW(state, maxCount)
}

func generateNewColor(state *gfxs.State) {
	state.RequestNewColor = false
	if state.Count < maxCount {
		state.Count++
	}
	state.Color = nk.NkRgb(rand.Int31n(256), rand.Int31n(256), rand.Int31n(256))
}
