module gitlab.com/azuwey/color_predictor

require (
	github.com/go-gl/gl v0.0.0-20180407155706-68e253793080
	github.com/go-gl/glfw v0.0.0-20181014061658-691ee1b84c51
	github.com/golang-ui/nuklear v0.0.0-20180627231830-4cfd78fa9597
	github.com/kpango/glg v1.2.1
)
