package gfxs

import (
	"fmt"

	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
	"github.com/kpango/glg"
)

func gfxMain(window *glfw.Window, context *nk.Context, state *State, maxCount *uint8) {
	nk.NkPlatformNewFrame()
	windowWidth, windowHeight := func() (float32, float32) {
		width, height := window.GetSize()
		return float32(width), float32(height)
	}()

	const titleHeight float32 = 40.0
	const boxPadding float32 = 4.0
	const topColorWindowX float32 = 0.0
	const topColorWindowY float32 = 0.0

	bottomWindowHeight := windowHeight / 5.0
	topWindowHeight := windowHeight - bottomWindowHeight

	// START DRAW TOP COLOR WINDOW
	colorPlaceBounds := nk.NkRect(topColorWindowX, topColorWindowY, windowWidth, topWindowHeight)
	if nk.NkBegin(context, "TOP_WINDOW", colorPlaceBounds, nk.WindowNoScrollbar) > 0 {
		// Get canvas
		canvas := nk.NkWindowGetCanvas(context)

		const rounding = 5.0

		blackRectanglePlaceX := boxPadding
		whiteRectanglePlaceX := (windowWidth / 2.0) + boxPadding

		rectangleWidth := (windowWidth / 2.0) - (2.0 * boxPadding)

		// Create a black rectangle
		blackPlaceBounds := nk.NkRect(blackRectanglePlaceX, boxPadding, rectangleWidth, topWindowHeight)
		nk.NkFillRect(canvas, blackPlaceBounds, rounding, nk.NkRgba(0, 0, 0, 255))

		// Create a white rectangle
		whitePlaceBounds := nk.NkRect(whiteRectanglePlaceX, boxPadding, rectangleWidth, topWindowHeight)
		nk.NkFillRect(canvas, whitePlaceBounds, rounding, nk.NkRgba(255, 255, 255, 255))

		const borderWidth float32 = 128.0

		blackRectangleX := blackRectanglePlaceX + (borderWidth / 2.0)
		whiteRectangleX := whiteRectanglePlaceX + (borderWidth / 2.0)

		coloredRectangleY := boxPadding + (borderWidth / 2.0)
		coloredRectangleHeight := topWindowHeight - borderWidth
		coloredRectangleWidth := rectangleWidth - borderWidth

		// Create a colored rectangle for the black background
		coloredBlackPlaceBounds := nk.NkRect(blackRectangleX, coloredRectangleY, coloredRectangleWidth, coloredRectangleHeight)
		nk.NkFillRect(canvas, coloredBlackPlaceBounds, 3, state.Color)

		// Create a colored rectangle for the white background
		coloredWhitePlaceBounds := nk.NkRect(whiteRectangleX, coloredRectangleY, coloredRectangleWidth, coloredRectangleHeight)
		nk.NkFillRect(canvas, coloredWhitePlaceBounds, 3, state.Color)
	}
	nk.NkEnd(context)
	// END DRAW TOP COLOR WINDOW

	// START DRAW BOTTOM BUTTON WINDOW
	buttonBarBounds := nk.NkRect(0, topWindowHeight, windowWidth, bottomWindowHeight)
	buttonBarTitle := fmt.Sprintf("[Color Predictor] Does Black or White look better over this color? %d/%d", *maxCount, state.Count)

	if nk.NkBegin(context, buttonBarTitle, buttonBarBounds, nk.WindowNoScrollbar|nk.WindowTitle) > 0 {
		nk.NkLayoutRowDynamic(context, bottomWindowHeight-titleHeight, 2)
		if nk.NkButtonLabel(context, "Black") > 0 {
			glg.Info("Black Button pressed")
			state.RequestNewColor = true
		}
		if nk.NkButtonLabel(context, "White") > 0 {
			glg.Info("White Button pressed")
			state.RequestNewColor = true
		}
	}
	nk.NkEnd(context)
	// END DRAW BOTTOM BUTTON WINDOW

	// RENDERING
	render(&windowWidth, &windowHeight, window)
}
