package gfxs

import (
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
	"github.com/kpango/glg"
)

func gfxPredict(window *glfw.Window, context *nk.Context, state *State, maxCount *uint8) {
	nk.NkPlatformNewFrame()
	windowWidth, windowHeight := func() (float32, float32) {
		width, height := window.GetSize()
		return float32(width), float32(height)
	}()

	const titleHeight float32 = 40.0
	const boxPadding float32 = 4.0
	const topColorWindowX float32 = 0.0
	const topColorWindowY float32 = 0.0

	bottomWindowHeight := windowHeight / 5.0
	topWindowHeight := windowHeight - bottomWindowHeight

	// START DRAW TOP COLOR WINDOW
	colorPlaceBounds := nk.NkRect(topColorWindowX, topColorWindowY, windowWidth, topWindowHeight)
	if nk.NkBegin(context, "TOP_WINDOW", colorPlaceBounds, nk.WindowNoScrollbar) > 0 {
		// Get canvas
		canvas := nk.NkWindowGetCanvas(context)

		const rounding = 5.0

		blackRectanglePlaceX := boxPadding

		rectangleWidth := windowWidth - (2.0 * boxPadding)

		// Create a background rectangle
		backgroundPlaceBounds := nk.NkRect(blackRectanglePlaceX, boxPadding, rectangleWidth, topWindowHeight)
		nk.NkFillRect(canvas, backgroundPlaceBounds, rounding, nk.NkRgba(0, 0, 0, 255))

		const borderWidth float32 = 128.0

		foregroundRectangleX := blackRectanglePlaceX + (borderWidth / 2.0)
		foregroundRectangleY := boxPadding + (borderWidth / 2.0)
		foregroundRectangleHeight := topWindowHeight - borderWidth
		foregroundRectangleWidth := rectangleWidth - borderWidth

		// Create a foreground rectangle for the background
		foregroundPlaceBounds := nk.NkRect(foregroundRectangleX, foregroundRectangleY, foregroundRectangleWidth, foregroundRectangleHeight)
		nk.NkFillRect(canvas, foregroundPlaceBounds, 3, state.Color)

	}
	nk.NkEnd(context)
	// END DRAW TOP COLOR WINDOW

	// START DRAW BOTTOM BUTTON WINDOW
	buttonBarBoundsLeft := nk.NkRect(0, topWindowHeight, windowWidth, bottomWindowHeight)
	if nk.NkBegin(context, "BOTTOM_WINDOW", buttonBarBoundsLeft, nk.WindowNoScrollbar) > 0 {
		nk.NkLayoutRowDynamic(context, (bottomWindowHeight-(boxPadding*3))/2, 2)
		if nk.NkButtonLabel(context, "Get new color") > 0 {
			glg.Info("Get new color Button pressed")
			state.RequestNewColor = true
		}
		r, g, b, _ := state.Color.RGBAi()
		r = nk.NkPropertyi(context, "#RED:", 0, r, 255, 1, 1)
		g = nk.NkPropertyi(context, "#GREEN:", 0, g, 255, 1, 1)
		b = nk.NkPropertyi(context, "#BLUE:", 0, b, 255, 1, 1)

		state.Color.SetRGBAi(r, g, b, 255)
	}
	nk.NkEnd(context)
	// END DRAW BOTTOM BUTTON WINDOW

	// RENDERING
	render(&windowWidth, &windowHeight, window)
}
