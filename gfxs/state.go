package gfxs

import (
	"github.com/golang-ui/nuklear/nk"
)

// State of the generated color
type State struct {
	Color           nk.Color
	Count           uint8
	ShouldExit      bool
	RequestNewColor bool
}
