package gfxs

import (
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
)

func render(width *float32, height *float32, window *glfw.Window) {
	// RENDERING
	bg := make([]float32, 4)
	nk.NkColorFv(bg, nk.NkRgba(0, 0, 0, 255))
	gl.Viewport(0, 0, int32(*width), int32(*height))
	gl.Clear(gl.COLOR_BUFFER_BIT)
	gl.ClearColor(bg[0], bg[1], bg[2], bg[3])
	nk.NkPlatformRender(nk.AntiAliasingOn, maxVertexBuffer, maxElementBuffer)
	window.SwapBuffers()
}
