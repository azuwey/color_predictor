package gfxs

import (
	"time"

	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/golang-ui/nuklear/nk"
	"github.com/kpango/glg"
)

const (
	// GLFW
	windowHeight     = 720
	windowWidth      = 1280
	maxVertexBuffer  = 512 * 1024
	maxElementBuffer = 128 * 1024
	windowTitle      = "Color Predictor"

	// Nuklear
	maxFps   = 60
	fontSize = 16
)

// InitGLFW is initialize the Graphics Library Framework
func InitGLFW(state *State, maxCount uint8) {
	if err := glfw.Init(); err != nil {
		glg.Fatal(err)
	}

	// Setup OpenGL properties
	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 2)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	glfw.WindowHint(glfw.Resizable, glfw.False)

	// Create a window with OpenGL flag
	window, err := glfw.CreateWindow(windowWidth, windowHeight, windowTitle, nil, nil)
	if err != nil {
		glg.Fatal(err)
	}

	// Setup OpenGL context
	window.MakeContextCurrent()

	// Set OpenGL viewport
	width, height := window.GetSize()
	if err := gl.Init(); err != nil {
		glg.Fatal(err)
	}
	gl.Viewport(0, 0, int32(width), int32(height))

	initNuklear(window, state, &maxCount)
}

// initNuklear is Initialize Nuklear GUI framework
func initNuklear(window *glfw.Window, state *State, maxCount *uint8) {
	context := nk.NkPlatformInit(window, nk.PlatformInstallCallbacks)
	atlas := nk.NewFontAtlas()
	nk.NkFontStashBegin(&atlas)
	sansFont := nk.NkFontAtlasAddDefault(atlas, fontSize, nil)
	nk.NkFontStashEnd()
	if sansFont != nil {
		nk.NkStyleSetFont(context, sansFont.Handle())
	}

	fpsTicker := time.NewTicker(time.Second / maxFps)
	for !window.ShouldClose() {
		select {
		case <-fpsTicker.C:
			glfw.PollEvents()
			if state.Count < *maxCount {
				gfxMain(window, context, state, maxCount)
			} else {
				gfxPredict(window, context, state, maxCount)
			}
			break
		}
	}

	fpsTicker.Stop()
	nk.NkPlatformShutdown()
	state.ShouldExit = true
}
