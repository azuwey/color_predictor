# Color Predictor

Color Predictor is an ML and OpenGL based application written in Go.

## Getting started

### First step: Download all the dependencies

```sh
make -C script get-dependencies
```

### Second step: Run the project

```sh
make -C scripts run
```